package ch.cern.todo.daos;

import ch.cern.todo.models.Tasks;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TasksDAO extends JpaRepository<Tasks,Long> {
}
