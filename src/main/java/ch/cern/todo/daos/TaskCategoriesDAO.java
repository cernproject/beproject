package ch.cern.todo.daos;

import ch.cern.todo.models.TaskCategories;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TaskCategoriesDAO extends JpaRepository<TaskCategories, Long> {
    TaskCategories findByCategoryName(String name);
}
