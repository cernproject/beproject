package ch.cern.todo.models;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;


@Entity
@Table(name = "tasks")
public class Tasks {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long taskId;
    private String taskName;
    private String taskDescription;
    private Date deadline;
    @OneToOne(cascade ={CascadeType.MERGE, CascadeType.DETACH , CascadeType.PERSIST})
    @JoinColumn(name = "category_id")
    private TaskCategories taskCategory;

    public Long getTaskId() {
        return taskId;
    }

    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public String getTaskDescription() {
        return taskDescription;
    }

    public void setTaskDescription(String taskDescription) {
        this.taskDescription = taskDescription;
    }

    public Date getDeadline() {
        return deadline;
    }

    public void setDeadline(Date deadline) {
        this.deadline = deadline;
    }

    public TaskCategories getTaskCategory() {
        return taskCategory;
    }

    public void setTaskCategory(TaskCategories taskCategory) {
        this.taskCategory = taskCategory;
    }
}
