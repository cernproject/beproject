package ch.cern.todo.controllers;

import ch.cern.todo.dtos.TaskCategoriesDTO;
import ch.cern.todo.services.TaskCategoriesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/task-categories")
public class TaskCategoriesController {

    @Autowired
    private TaskCategoriesService taskCategoriesService;

    @PostMapping("/create")
    public TaskCategoriesDTO createTaskCategory(@RequestBody TaskCategoriesDTO taskCategory) {
        return taskCategoriesService.createTaskCategory(taskCategory);

    }

    @GetMapping("/getCategoryByName/{name}")
    public TaskCategoriesDTO getCategoryByName(@PathVariable String name) {
        return taskCategoriesService.getCategoryByName(name);

    }

    @PutMapping("/update")
    public TaskCategoriesDTO updateTaskCategory(@RequestBody TaskCategoriesDTO taskCategory) {
        return taskCategoriesService.updateTaskCategory(taskCategory);
    }

    @DeleteMapping("/delete")
    public void deleteTaskCategory(@RequestBody TaskCategoriesDTO taskCategory) {
        taskCategoriesService.deleteTaskCategory(taskCategory);
    }
}
