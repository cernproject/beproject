package ch.cern.todo.controllers;

import ch.cern.todo.dtos.TaskCategoriesDTO;
import ch.cern.todo.dtos.TasksDTO;
import ch.cern.todo.services.TaskCategoriesService;
import ch.cern.todo.services.TasksServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/tasks")
public class TasksController {

    @Autowired
    private TasksServices tasksServices;

    @PostMapping("/create")
    public TasksDTO createTask(@RequestBody TasksDTO tasksDTO) {
        return tasksServices.createTask(tasksDTO);

    }

    @GetMapping("/getAllTasks")
    public List<TasksDTO> getAllTasks() {
        return tasksServices.getAllTasks();

    }

    @PutMapping("/update")
    public TasksDTO updateTask(@RequestBody TasksDTO tasksDTO) {
        return tasksServices.updateTask(tasksDTO);
    }

    @DeleteMapping("/delete")
    public void deleteTask(@RequestBody TasksDTO tasksDTO) {
        tasksServices.deleteTask(tasksDTO);
    }
}
