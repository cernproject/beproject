package ch.cern.todo.config;

import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeanConfig {

    // singleton object will be created by spring context
    @Bean
    public ModelMapper modelMapper() {
        ModelMapper modelMapper =  new ModelMapper();
        //stops overwriting values on destination, when source value is null
        modelMapper.getConfiguration().setSkipNullEnabled(true);
        return modelMapper;
    }
}
