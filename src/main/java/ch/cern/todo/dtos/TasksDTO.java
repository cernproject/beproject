package ch.cern.todo.dtos;

import ch.cern.todo.models.TaskCategories;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

public class TasksDTO {


    private long taskId;
    private String taskName;
    private String taskDescription;
    private Date deadline;
    private TaskCategoriesDTO taskCategoryDTO;

    public long getTaskId() {
        return taskId;
    }

    public void setTaskId(long taskId) {
        this.taskId = taskId;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskyName) {
        this.taskName = taskyName;
    }

    public String getTaskDescription() {
        return taskDescription;
    }

    public void setTaskDescription(String taskDescription) {
        this.taskDescription = taskDescription;
    }

    public Date getDeadline() {
        return deadline;
    }

    public void setDeadline(Date deadline) {
        this.deadline = deadline;
    }

    public TaskCategoriesDTO getTaskCategoryDTO() {
        return taskCategoryDTO;
    }

    public void setTaskCategoryDTO(TaskCategoriesDTO taskCategoryDTO) {
        this.taskCategoryDTO = taskCategoryDTO;
    }
}
