package ch.cern.todo.services;

import ch.cern.todo.dtos.TaskCategoriesDTO;

public interface TaskCategoriesService {
    TaskCategoriesDTO createTaskCategory(TaskCategoriesDTO taskCategory);

    TaskCategoriesDTO getCategoryByName(String name);

    TaskCategoriesDTO updateTaskCategory(TaskCategoriesDTO taskCategory);

    void deleteTaskCategory(TaskCategoriesDTO taskCategory);
}
