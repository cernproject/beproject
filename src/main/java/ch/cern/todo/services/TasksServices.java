package ch.cern.todo.services;

import ch.cern.todo.dtos.TasksDTO;

import java.util.List;

public interface TasksServices {
    TasksDTO createTask(TasksDTO tasksDTO);

    List<TasksDTO> getAllTasks();

    TasksDTO updateTask(TasksDTO tasksDTO);

    void deleteTask(TasksDTO tasksDTO);
}
