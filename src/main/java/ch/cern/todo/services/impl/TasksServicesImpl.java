package ch.cern.todo.services.impl;

import ch.cern.todo.daos.TaskCategoriesDAO;
import ch.cern.todo.daos.TasksDAO;
import ch.cern.todo.dtos.TaskCategoriesDTO;
import ch.cern.todo.dtos.TasksDTO;
import ch.cern.todo.models.TaskCategories;
import ch.cern.todo.models.Tasks;
import ch.cern.todo.services.TasksServices;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class TasksServicesImpl implements TasksServices {

    @Autowired
    private TasksDAO tasksDAO;
    @Autowired
    private ModelMapper modelMapper;

    @Override
    public TasksDTO createTask(TasksDTO tasksDTO) {
        Tasks taskModel = modelMapper.map(tasksDTO,Tasks.class);
        taskModel = tasksDAO.save(taskModel);
        TaskCategoriesDTO taskCategoriesDTO = modelMapper.map(taskModel.getTaskCategory(),TaskCategoriesDTO.class);
        tasksDTO = modelMapper.map(taskModel, TasksDTO.class);
        tasksDTO.setTaskCategoryDTO(taskCategoriesDTO);
        return tasksDTO;
    }

    @Override
    public List<TasksDTO> getAllTasks() {
        List<Tasks> listOfTasks = tasksDAO.findAll();
        return listOfTasks.stream()
                .map(task -> modelMapper.map(task,TasksDTO.class))
                .collect(Collectors.toList());
    }

    @Override
    public TasksDTO updateTask(TasksDTO tasksDTO) {
        Tasks taskModel = modelMapper.map(tasksDTO,Tasks.class);
        return modelMapper.map(tasksDAO.save(taskModel), TasksDTO.class);
    }

    @Override
    public void deleteTask(TasksDTO tasksDTO) {
        Tasks taskModel = modelMapper.map(tasksDTO,Tasks.class);
        tasksDAO.delete(taskModel);
    }
}
