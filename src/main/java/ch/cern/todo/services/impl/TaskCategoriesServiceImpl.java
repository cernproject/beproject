package ch.cern.todo.services.impl;

import ch.cern.todo.daos.TaskCategoriesDAO;
import ch.cern.todo.dtos.TaskCategoriesDTO;
import ch.cern.todo.models.TaskCategories;
import ch.cern.todo.services.TaskCategoriesService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TaskCategoriesServiceImpl implements TaskCategoriesService {

    @Autowired
    private TaskCategoriesDAO taskCategoriesDAO;
    @Autowired
    private ModelMapper modelMapper;

    public TaskCategoriesDTO createTaskCategory(TaskCategoriesDTO taskCategory) {
        TaskCategories taskCategoryModel = modelMapper.map(taskCategory,TaskCategories.class);
        return modelMapper.map(taskCategoriesDAO.save(taskCategoryModel),TaskCategoriesDTO.class);
    }

    @Override
    public TaskCategoriesDTO getCategoryByName(String name) {
        return modelMapper.map(taskCategoriesDAO.findByCategoryName(name),TaskCategoriesDTO.class);
    }

    @Override
    public TaskCategoriesDTO updateTaskCategory(TaskCategoriesDTO taskCategory) {
        TaskCategories taskCategoryModel = modelMapper.map(taskCategory,TaskCategories.class);
        return modelMapper.map(taskCategoriesDAO.save(taskCategoryModel),TaskCategoriesDTO.class);
    }

    @Override
    public void deleteTaskCategory(TaskCategoriesDTO taskCategory) {
        TaskCategories taskCategoryModel = modelMapper.map(taskCategory,TaskCategories.class);
        taskCategoriesDAO.delete(taskCategoryModel);
    }
}
